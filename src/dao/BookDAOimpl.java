package dao;

import pojo.Book;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
public class BookDAOimpl  extends Thread implements BookDAO{
	
	private Scanner sc;
	private ArrayList<Book> booklist;
	private ArrayList<Book> newbooks;
	private ArrayList<Book> favbooks;
	private ArrayList<Book> completedbooks;
	public BookDAOimpl()
	{
		sc=new Scanner(System.in);
		//creating arraylist for booklist, newbooks, favbooks, completedbooks
		booklist=new ArrayList<Book>();
		newbooks=new ArrayList<Book>();
		favbooks=new ArrayList<Book>();
		completedbooks=new ArrayList<Book>();
		//creating object for book class
		Book b1=new Book();
		//Storing default details in book list
		b1.setBookName("The india story ");
		b1.setAuthorname("Bimal jalal");
		b1.setDescription("The history of india");
		b1.setBookId(1);
		booklist.add(b1);
		Book b2=new Book();
		b2.setBookName("The little book of Encouragement");
		b2.setAuthorname("Dalai lama");
		b2.setDescription("The book give the Encouragement in life");
		b2.setBookId(2);
		booklist.add(b2);
		Book b3=new Book();
		b3.setBookName(" c language ");
		b3.setAuthorname("Dennis Ritche");
		b3.setDescription("C is a mother language");
		b3.setBookId(3);
		booklist.add(b3);
		Book b4=new Book();
		b4.setBookName("java ");
		b4.setAuthorname("Balaguruswamy");
		b4.setDescription("The book contails oops content");
		b4.setBookId(4);
		booklist.add(b4);
		Book b5=new Book();
		b5.setBookName(" A Better india ");
		b5.setAuthorname("Narayana Murthy");
		b5.setDescription("The content of india achieved things");
		b5.setBookId(5);
		booklist.add(b5);
		Book b6=new Book();
		b6.setBookName("A Suitable Boy ");
		b6.setAuthorname("Vikram Seth");
		b6.setDescription("The story of a boy");
		b6.setBookId(6);
		booklist.add(b5);
		//Adding books in newbooks list
		newbooks.add(b1);
		newbooks.add(b2);
		//Adding books in favbooks list
		favbooks.add(b3);
		favbooks.add(b4);
		//Adding books in completed books list
		completedbooks.add(b5);
		completedbooks.add(b6);
		
	}
	
	@Override
	public void newbooks() //Displaying all new books
	{
		
		System.out.println("-----The  New Book List are------");
		
		for(Book b :newbooks) 
		 {
			

			 System.out.println("BookID:	  "+b.getBookId());
			 System.out.println("Bookname:    "+b.getBookName());
			 System.out.println("Authorname:  "+b.getAuthorname());
			 System.out.println("Discription: "+b.getDescription());
			 System.out.println("");
		
		 } 
			
				
		
		
	}

	@Override
	public void favbooks()//Displaying all favourite books 
	{
		
		System.out.println("-----The  FavBook List are------");
		
		 for(Book b :favbooks) 
		 {		//loop for printing book details by using bookid

			 System.out.println("BookID:	  "+b.getBookId());
			 System.out.println("Bookname:    "+b.getBookName());
			 System.out.println("Authorname:  "+b.getAuthorname());
			 System.out.println("Discription: "+b.getDescription());
			 System.out.println("");
		 }
		
	}

	@Override
	public void completedbooks() //Displaying all completedbooks
	{
		
		System.out.println("-----The  Completed Book List are------");
		
		 for(Book b :completedbooks) 
		 {		//loop for printing book details by using book id

			 System.out.println("BookID:	  "+b.getBookId());
			 System.out.println("Bookname:    "+b.getBookName());
			 System.out.println("Authorname:  "+b.getAuthorname());
			 System.out.println("Discription: "+b.getDescription());
			 System.out.println("");
		 }
		
		
		
	}

	@Override
	//method to select book id to get book details if present in booklist
	public void selectbookid()throws NegitiveException
	{
		 System.out.println("Enter book id to select");
		 int id=sc.nextInt();
		 int flag=0;
		 if(id<0)
		 {
		 throw new NegitiveException();
		 }
		 System.out.println("-----The selected BookList details are------");
		
		 for(Book b :booklist) 
		 {					 
			 if(b.getBookId()==(id)) 
			 {

				 System.out.println("BookID:	  "+b.getBookId());
				 System.out.println("Bookname:    "+b.getBookName());
				 System.out.println("Authorname:  "+b.getAuthorname());
				 System.out.println("Discription: "+b.getDescription());
				 System.out.println();
				 flag=1;
			 } 
			 
		 }
		 if(flag==1)
			 System.out.println("BookId found the Details of books are given above");
		 else
			 System.out.println("BookId not found");
		 	 System.out.println("Enter book id from 1..to...6");
		
	}

	@Override
	public void viewallbooks() //Displaying all books in magic of bookstore
	{
		
		for(Book b2 :booklist) 
		 {	
				 System.out.println("BookID:	  "+b2.getBookId());
				 System.out.println("Bookname:    "+b2.getBookName());
				 System.out.println("Authorname:  "+b2.getAuthorname());
				 System.out.println("Discription: "+b2.getDescription());
				 System.out.println();
			 
		 }
	}
	
	}
	
