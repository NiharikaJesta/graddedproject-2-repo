package mainpackage;
import dao.*;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import dao.*;

public class Main extends Thread  //Extending Thread class in main class
{
	public static Scanner sc;
	//constructor
	public Main()
	{
	sc=new Scanner(System.in);
	}
	
//main method to run the thread
	
public void run()
{
	UserDAOimpl user=new UserDAOimpl();
	//boolean result=user.verifyusername();
	//if(result)
	//{
	  user.verifyusername();
	  BookDAOimpl book=new BookDAOimpl();
	  System.out.println("--------Magic of Book--------");
	  String ch="y";
	  while(ch.equals("y"))
	  {
		System.out.println("select options give below");
		System.out.println("1.view all books\n2.New books\n3.Favourite books\n4.Completed books\n5.Select book id");
		int option=sc.nextInt();
		switch(option)
		{
		case 1:
			book.viewallbooks();
			break;
		case 2:
			book.newbooks();
			break;
		case 3:
			book.favbooks();
			break;
		case 4:
			book.completedbooks();
			break;
		case 5:
			try {
				book.selectbookid();
			} catch (NegitiveException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 0:
			System.exit(0);
		default:
            System.out.println("Enter valid choice (0..5)");
		}
		System.out.println("Do u want to continue choose(y/n)");
		ch=sc.next();
	}
//}
	/*else
	{
		System.out.println("Invalid User Details....Enter the valid credintials ");
	}*/
}	

public static void main(String[] args)throws Exception
{
	//using loggers
    boolean append = true;
    FileHandler handler = new FileHandler("new.log", append);

    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    logger.addHandler(handler);
     
    logger.severe("severe message");

    logger.warning("warning message");

    logger.info("info message");

    logger.config("config message");

    logger.fine("fine message");

    logger.finer("finer message");

    logger.finest("finest message");
    //object creating for the class Main 
    Main m=new Main();
    m.start();//call thread to start

}

}
