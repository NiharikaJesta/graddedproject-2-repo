package pojo;

import java.util.Objects;

public class Book 
{

	private String bookName;
	private String authorname;
	private String description;
	private int bookId;

	

	public String getBookName() {
		return bookName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(authorname, bookId, bookName, description);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return Objects.equals(authorname, other.authorname) && bookId == other.bookId
				&& Objects.equals(bookName, other.bookName) && Objects.equals(description, other.description);
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	

	

	
	
	
}
